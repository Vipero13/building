using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    private Renderer ceiling;
    private Animator animBl, animBa, animCa, animCe, animDe, animF1, animFl, animGa, animGr, animRo, animSt;
    private TextMeshProUGUI title;
    private int step = 0;

    // Start is called before the first frame update
    void Start()
    {
        title = GameObject.Find("Title").GetComponent<TextMeshProUGUI>();

        ceiling = GameObject.Find("Ceiling1").GetComponent<Renderer>();

        animBl = GameObject.Find("Blocking_Building").GetComponent<Animator>();
        animBa = GameObject.Find("Basement").GetComponent<Animator>();
        animCa = GameObject.Find("Camera").GetComponent<Animator>();
        animCe = GameObject.Find("Ceiling1").GetComponent<Animator>();
        animDe = GameObject.Find("Details").GetComponent<Animator>();
        animF1 = GameObject.Find("F1").GetComponent<Animator>();
        animFl = GameObject.Find("Plane").GetComponent<Animator>();
        animGa = GameObject.Find("Galleria1FRoof").GetComponent<Animator>();
        animGr = GameObject.Find("Ground").GetComponent<Animator>();
        animRo = GameObject.Find("RoofGallery").GetComponent<Animator>();
        animSt = GameObject.Find("Stairs").GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) && step < 8) AnimateStep(++step);
        else if (Input.GetKeyDown(KeyCode.LeftArrow) && step > 0) AnimateStep(--step);

        if (Input.GetMouseButtonDown(0))
        {
            if (step < 8 && Input.mousePosition.x > Screen.width / 2) AnimateStep(++step);

            else if (step > 0 && Input.mousePosition.x <= Screen.width / 2) AnimateStep(--step);
        }
    }

    private void AnimateStep(int i)
    {
        switch (i)
        {
            case 0:
                title.text = "Locate";
                animBl.Play("Step0_Blocking");
                animFl.Play("Step0_Map");
                animBa.Play("Step0_Basement");
                animCa.Play("Step0_Camera");
                animCe.Play("Step0_Ceiling");
                animDe.Play("Step0_Details");
                animF1.Play("Step0_F1");
                animGa.Play("Step0_Galleria1FRoof");
                animGr.Play("Step0_Ground");
                animRo.Play("Step0_RoofGallery");
                animSt.Play("Step0_Stairs");
                break;
            case 1:
                title.text = "Open Up";
                animBl.Play("Step1A_Blocking");
                break;
            case 2:
                title.text = "Adapt";
                animFl.Play("Step0_Map");
                animBl.Play("Step1B_Blocking");
                animDe.Play("Step0_Details");
                animGr.Play("Step0_Ground");
                break;
            case 3:
                title.text = "Activate";
                animBl.Play("Step2_Blocking");
                animDe.Play("Step2_Details");
                animFl.Play("Step2_Map");
                animBa.Play("Step0_Basement");
                animCa.Play("Step0_Camera");
                animCe.Play("Step0_Ceiling");
                animF1.Play("Step0_F1");
                animSt.Play("Step0_Stairs");
                break;
            case 4:
                title.text = "Volume Usage";
                animDe.Play("Step2a_Details");
                break;
            case 5:
                title.text = "Base Volumes";
                animBa.Play("Step3_Basement");
                animCa.Play("Step3_Camera");
                animCe.Play("Step3_Ceiling");
                animDe.Play("Step3_Details");
                animF1.Play("Step3_F1");
                animFl.Play("Step3_Map");
                animGr.Play("Step3_Ground");
                animSt.Play("Step3_Stairs");
                animRo.Play("Step0_RoofGallery");
                ceiling.enabled = false;
                break;
            case 6:
                title.text = "Base Flat Roof";
                ceiling.enabled = true;
                animBa.Play("Step4_Basement");
                animF1.Play("Step4_F1");
                animDe.Play("Step4_Details");
                animRo.Play("Step4_RoofGallery");
                animGr.Play("Step4_Ground");
                animGa.Play("Step0_Galleria1FRoof");
                break;
            case 7:
                title.text = "Attraction Points";
                animRo.Play("Step5_RoofGallery");
                break;

            case 8:
                title.text = "Final Morphology";
                animGa.Play("Step5_Galleria1FRoof");
                break;
            default:
                Debug.Log("Incorrect step number.");
                break;
        }
    }
}